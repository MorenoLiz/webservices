﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    interface IPersona
    {

        IEnumerable<Persona> GetAll();
        Persona Get(int idPersona);
        Persona Add(Persona persona);
        void Remove(int idPersona);
        bool Update(Persona persona);
    }
}
