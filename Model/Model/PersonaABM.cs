﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    class PersonaABM : IPersona
    {
        private List<Persona> listaPersonas = new List<Persona>();
        private int _nextId = 1;



        public IEnumerable<Persona> GetAll()
        {
            return listaPersonas;
        }

        public Persona Get(int id)
        {
            return listaPersonas.Find(p => p.Id == id);
        }

        public Persona Add(Persona persona)
        {
            if (persona == null)
            {
                throw new ArgumentNullException("persona");
            }

            persona.Id = _nextId++;
            listaPersonas.Add(persona);
            return persona;
        }

        public void Remove(int id)
        {
            listaPersonas.RemoveAll(p => p.Id == id);
        }

        public bool Update(Persona persona)
        {
            if (persona == null)
            {
                throw new ArgumentNullException("persona");
            }
            int index = listaPersonas.FindIndex(p => p.Id == persona.Id);
            if (index == -1)
            {
                return false;
            }
            listaPersonas.RemoveAt(index);
            listaPersonas.Add(persona);
            return true;
        }
    }
}
