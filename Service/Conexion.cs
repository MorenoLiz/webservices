﻿#region snippet_all
using Model;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Service
{
    public class Conexion
    {
        #region snippet_prod
        Persona persona = new Persona();
        #endregion

        #region snippet_HttpClient
        HttpClient client = new HttpClient();
        #endregion

        public void ShowPersona(Persona persona)
        {
            Console.WriteLine($"Nombre: {persona.Nombre}\tApellido: " +
                $"{persona.Apellido}\tTipo de documento: " +
                $"{persona.TipoDocumento}\tDNI: {persona.Documento}\tFecha de nacimiento: " +
                $" { persona.FechaNacimiento}");
        }

        #region snippet_CreatePersonaAsync
        public async Task<Uri> CreatePersonaAsync(Persona persona)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync("api/prueba", persona);

            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }
        #endregion

        #region snippet_GetPersonaAsync
        public async Task<Persona> GetPersonaAsync(string path)
        {
            Persona persona = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                persona = await response.Content.ReadAsAsync<Persona>();
            }
            return persona;
        }
        #endregion

        #region snippet_UpdatePersonaAsync
        public async Task<Persona> UpdatePersonaAsync(Persona persona)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync($"api/prueba/{persona.Id}", persona);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            persona = await response.Content.ReadAsAsync<Persona>();
            return persona;
        }
        #endregion

        #region snippet_DeletePersonaAsync
        public async Task<HttpStatusCode> DeletePersonaAsync(int id)
        {
            HttpResponseMessage response = await client.DeleteAsync(
                $"api/prueba/{id}");
            return response.StatusCode;
        }
        #endregion

        public void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        #region snippet_run
        #region snippet5
        public async Task RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("https://----");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            #endregion

            try
            { /*
                // Create a new persona
                Persona persona = new Persona
                {
                    Nombre = "Pamela",
                    Apellido = "Paz",
                    TipoDocumento = "DNI",
                    Documento = "30.676.876",
                    FechaNacimiento = new DateTime(1980, 3, 1, 7, 0, 0)
                

                };

             
                var url = await CreatePersonaAsync(persona);
               */
              //Console.WriteLine($"Created at {url}");
              /*
           // Get the product
           persona = await GetPersonaAsync(url.PathAndQuery);
           ShowPersona(persona);
           */


                // Update the product
                Console.WriteLine("Updating...");
                //persona.Nombre = "Kiara";
                var url = "api/prueba/6";
                Persona persona = await GetPersonaAsync(url);
                persona.Nombre = "kiara";
                persona.Apellido = "moreno";
                persona = await UpdatePersonaAsync(persona);


                /*
                // Get the updated product
                persona = await GetPersonaAsync(url.PathAndQuery);
                ShowPersona(persona);
                */


                /*
                // Delete the product
                var statusCode = await DeletePersonaAsync(7);
                Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");
                */

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
        #endregion
    }
}
#endregion
